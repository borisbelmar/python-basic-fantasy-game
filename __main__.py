from data import hero, monsters
from actions import combat, equip_weapon, equip_armor, get_monster_by_id
from cli import print_creature_stats

monster = get_monster_by_id(3)

equip_weapon(hero, 1)
equip_armor(hero, 1)
print_creature_stats(hero)

# print(monster)
# print(hero)

# combat(monster, hero)

# print('After')
# print(monster)
# print(hero)

# equip_weapon(hero, 0)
# equip_armor(monster, 0)
# combat(monster, hero)

# print('After 2')
# print(f'{monster["hp"]}/{monster["max_hp"]}')
# print(f'{hero["hp"]}/{hero["max_hp"]}')