from actions import get_total_atk, get_total_def

def print_creature_stats(creature):
  print(f'------ Estadísticas de {creature["name"]} -------')
  print(f'Nombre: {creature["name"]}')
  print(f'HP: {creature["hp"]}/{creature["max_hp"]}')
  print(f'ATK: {get_total_atk(creature)}')
  print(f'DEF: {get_total_def(creature)}')
  print(f'Arma: {"Ninguna" if not creature["weapon"] else creature["weapon"]["name"]}')
  print(f'Armadura: {"Ninguna" if not creature["armor"] else creature["armor"]["name"]}')
  print(f'-------------------------------------------------')