monsters = [
  {
    "id": 1,
    "name": "Slime",
    "hp": 10,
    "max_hp": 10,
    "atk": 1,
    "def": 0,
    "weapon": None,
    "armor": None
  },
  {
    "id": 2,
    "name": "Esqueleto",
    "hp": 15,
    "max_hp": 15,
    "atk": 2,
    "def": 1,
    "weapon": None,
    "armor": None
  },
  {
    "id": 3,
    "name": "Goblin",
    "hp": 10,
    "max_hp": 10,
    "atk": 3,
    "def": 0,
    "weapon": None,
    "armor": None
  }
]