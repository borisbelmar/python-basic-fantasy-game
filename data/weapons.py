weapons = [
  {
    "id": 1,
    "name": "Wooden Sword",
    "atk": 1
  },
  {
    "id": 2,
    "name": "Dagger",
    "atk": 2
  },
  {
    "id": 3,
    "name": "Broadsword",
    "atk": 3
  },
  {
    "id": 4,
    "name": "Atomic Gun",
    "atk": 100
  }
]