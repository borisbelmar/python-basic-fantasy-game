armors = [
  {
    "id": 1,
    "name": "Wooden Armor",
    "def": 1
  },
  {
    "id": 2,
    "name": "Copper Armor",
    "def": 2
  },
  {
    "id": 3,
    "name": "Silver Armor",
    "def": 3
  },
  {
    "id": 4,
    "name": "SuperMega Armor",
    "def": 100
  }
]