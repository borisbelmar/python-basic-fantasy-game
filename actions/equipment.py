from data import weapons, armors
from utils import find_by_id

def equip_weapon(creature, weapon_id):
  creature['weapon'] = find_by_id(weapon_id, weapons)

def unequip_weapon(creature):
  creature['weapon'] = None

def equip_armor(creature, armor_id):
  creature['armor'] = find_by_id(armor_id, armors)

def unequip_armor(creature):
  creature['armor'] = None