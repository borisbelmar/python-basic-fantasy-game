from utils import find_by_id
from data import weapons, monsters

def get_weapon_by_id(id):
  return find_by_id(id, weapons)

def get_monster_by_id(id):
  return find_by_id(id, monsters)