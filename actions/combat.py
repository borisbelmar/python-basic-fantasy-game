def get_total_atk(creature):
  if creature['weapon']:
    return creature['atk'] + creature['weapon']['atk']
  else:
    return creature['atk']
  
def get_total_def(creature):
  if creature['armor']:
    return creature['def'] + creature['armor']['def']
  else:
    return creature['def']

def _get_damage(defender, atacker):
  dmg = defender['hp'] - (get_total_atk(atacker) - get_total_def(defender))
  if (dmg > 0):
    return dmg
  else:
    return 0

def _get_heal(creature, heal_amount):
  new_hp = creature['hp'] + heal_amount
  if (new_hp <= creature['max_hp']):
    return new_hp
  else:
    return creature['max_hp']

def combat(defender, atacker):
  defender['hp'] = _get_damage(defender, atacker)

def heal(creature, heal_amount):
  creature['hp'] = _get_heal(creature, heal_amount)
