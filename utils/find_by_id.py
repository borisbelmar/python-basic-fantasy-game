def find_by_id (id, listOfDictionaries):
  return next((element for element in listOfDictionaries if element['id'] == id), False)